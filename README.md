dwm_frames - Some personal customization
=========================================

This repo is an attempt to make a more human-controlled layout
for dwm, a framed layout.

- The goal is to be able to construct on-the-fly a customized
layout that fits the current needs.

- An important side goal is to be able to reconstruct with that method
the useful layouts we can see in a patched dwm
(things like xtile, gaplessgrid, centeredmaster, ...)

- A minor side goal is to allow the user to freely pass
between a classical dwm layout and something like i3 tiling.


**This is still a mess.**
This is currently a back-up of my customized dwm: you will
find all the patches I use, not only the framed layout.

What is there:

- Framed layout (of course)
- Transparency support (quite experimental patch, according to it's author)
- Shiftview (personal one)
- Gapless grid layout
- Tag set extension (you have 4 instead of 2 tagset to play with)
- Dual status bar


You'll find below the original README

dwm - dynamic window manager
============================
dwm is an extremely fast, small, and dynamic window manager for X.


Requirements
------------
In order to build dwm you need the Xlib header files.


Installation
------------
Edit config.mk to match your local setup (dwm is installed into
the /usr/local namespace by default).

Afterwards enter the following command to build and install dwm (if
necessary as root):

    make clean install

If you are going to use the default bluegray color scheme it is highly
recommended to also install the bluegray files shipped in the dextra package.


Running dwm
-----------
Add the following line to your .xinitrc to start dwm using startx:

    exec dwm

In order to connect dwm to a specific display, make sure that
the DISPLAY environment variable is set correctly, e.g.:

    DISPLAY=foo.bar:1 exec dwm

(This will start dwm on display :1 of the host foo.bar.)

In order to display status info in the bar, you can do something
like this in your .xinitrc:

    while xsetroot -name "`date` `uptime | sed 's/.*,//'`"
    do
    	sleep 1
    done &
    exec dwm


Configuration
-------------
The configuration of dwm is done by creating a custom config.h
and (re)compiling the source code.
