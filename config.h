/* See LICENSE file for copyright and license details. */

/* appearance */
static const char *fonts[] = {
	"monospace:size=10"
};
static const char dmenufont[]       = "monospace:size=10";
static const char normbordercolor[] = "#444444";
static const char normbgcolor[]     = "#222222";
static const char normfgcolor[]     = "#bbbbbb";
static const char selbordercolor[]  = "#005577";
static const char selbgcolor[]      = "#333377";
static const char selfgcolor[]      = "#eeeeee";
/* Extra */
static const char selframebordercolor[]  = "#986d29";
static const char selframebgcolor[]      = "#986d29";
static const char selframefgcolor[]      = "#eeeeee";
/* ----- */
static const unsigned int borderpx  = 1;        /* border pixel of windows */
static const unsigned int snap      = 32;       /* snap pixel */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */
static const int extrabar           = 1;        /* 0 means no extra bar */
static const double defaultopacity  = 0.75;

/* tagging */
static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9" };

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class      instance    title       tags mask     isfloating   monitor */
	{ "Gimp",     NULL,       NULL,       0,            1,           -1 },
	{ "Firefox",  NULL,       NULL,       1 << 8,       0,           -1 },
};

/* layout(s) */
#include "gaplessgrid.c"
static const float mfact     = 0.55; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 1;    /* 1 means respect size hints in tiled resizals */

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "F1v",      framedtile},
	{ "[]=",      tile },    /* first entry is default */
	{ "><>",      NULL },    /* no layout function means floating behavior */
	{ "[M]",      monocle },
	{ "###",      gaplessgrid },
};

static const FrameLayout framelayouts[] = {
	{ "F0v",     inframe_vertical },
	{ "F0>",     inframe_horizontal },
	{ "F0M",     inframe_monocle },
	{ "F0#",     inframe_grid },
};

/* key definitions */
#define MODKEY Mod1Mask
#define FRAMEKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", "-m", dmenumon, "-fn", dmenufont, "-nb", normbgcolor, "-nf", normfgcolor, "-sb", selbgcolor, "-sf", selfgcolor, NULL };
static const char *termcmd[]  = { "st", NULL };

static Key keys[] = {
	/* modifier                     key        function        argument */
	{ MODKEY,                       XK_p,      spawn,          {.v = dmenucmd } },
	{ MODKEY|ShiftMask,             XK_Return, spawn,          {.v = termcmd } },
	{ MODKEY,                       XK_b,      togglebar,      {0} },
	{ MODKEY|ShiftMask,             XK_b,      toggleextrabar, {0} },
	{ MODKEY,                       XK_j,      focusstack,     {.i = +1 } },
	{ MODKEY,                       XK_k,      focusstack,     {.i = -1 } },
	{ MODKEY,                       XK_i,      incnmaster,     {.i = +1 } },
	{ MODKEY,                       XK_d,      incnmaster,     {.i = -1 } },
	{ MODKEY,                       XK_h,      setmfact,       {.f = -0.05} },
	{ MODKEY,                       XK_l,      setmfact,       {.f = +0.05} },
	{ MODKEY,                       XK_Return, zoom,           {0} },
	{ MODKEY,                       XK_Tab,    view,           {0} },
	{ MODKEY|ShiftMask,             XK_c,      killclient,     {0} },
	{ MODKEY,                       XK_t,      setlayout,      {.v = &layouts[1]} },
	{ MODKEY,                       XK_f,      setlayout,      {.v = &layouts[2]} },
	{ MODKEY,                       XK_m,      setlayout,      {.v = &layouts[3]} },
	{ MODKEY|ShiftMask,             XK_t,      setlayout,      {.v = &layouts[4]} },
	{ MODKEY|ControlMask,           XK_t,      setlayout,      {.v = &layouts[0]} },
	{ MODKEY,                       XK_space,  setlayout,      {0} },
	{ MODKEY|ShiftMask,             XK_space,  togglefloating, {0} },
	{ MODKEY,                       XK_0,      view,           {.ui = ~0 } },
	{ MODKEY|ShiftMask,             XK_0,      tag,            {.ui = ~0 } },
	{ MODKEY,                       XK_comma,  focusmon,       {.i = -1 } },
	{ MODKEY,                       XK_period, focusmon,       {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_comma,  tagmon,         {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_period, tagmon,         {.i = +1 } },
	TAGKEYS(                        0x26,                      0)
	TAGKEYS(                        0xe9,                      1)
	TAGKEYS(                        0x22,                      2)
	TAGKEYS(                        0x27,                      3)
	TAGKEYS(                        0x28,                      4)
	TAGKEYS(                        0x2d,                      5)
	TAGKEYS(                        0xe8,                      6)
	TAGKEYS(                        0x5f,                      7)
	TAGKEYS(                        0xe7,                      8)
	{ MODKEY|ShiftMask,             XK_q,      quit,           {0} },
	{ MODKEY|ShiftMask,             XK_Tab,    popview,        {.ui = 2} },
	/*{ MODKEY|ControlMask,           XK_Left,      shiftview,   {.i = -1} },
	{ MODKEY|ControlMask,           XK_Right,     shiftview,   {.i = 1}  },
	{ MODKEY|ControlMask,           XK_Down,      shiftview,   {.i = -2} },
	{ MODKEY|ControlMask,           XK_Up,        shiftview,   {.i = 2}  },*/
	{ MODKEY|ControlMask,           XK_Page_Down, shiftview,   {.i = -1} },
	{ MODKEY|ControlMask,           XK_Page_Up,   shiftview,   {.i = 1}  },
	{ MODKEY|ShiftMask,             XK_Page_Down, spawn,       SHCMD("transset-df -a --dec .1") },
	{ MODKEY|ShiftMask,             XK_Page_Up, spawn,         SHCMD("transset-df -a --inc .1") },
	{ MODKEY|ShiftMask,             XK_Home,    spawn,         SHCMD("transset-df -a .75") },
	{ FRAMEKEY,             XK_j,       focusframe,    {.i = +1} },
	{ FRAMEKEY,             XK_k,       focusframe,    {.i = -1} },
	{ FRAMEKEY|ShiftMask,             XK_Left,    divideframe,   {.ui = 2} },
	{ FRAMEKEY|ShiftMask,             XK_Right,   divideframe,   {.ui = 0} },
	{ FRAMEKEY|ShiftMask,             XK_Up,      divideframe,   {.ui = 1} },
	{ FRAMEKEY|ShiftMask,             XK_Down,    divideframe,   {.ui = 3} },
	{ FRAMEKEY|ControlMask|ShiftMask, XK_Left,    collapseframe, {.ui = 2} },
	{ FRAMEKEY|ControlMask|ShiftMask, XK_Right,   collapseframe, {.ui = 0} },
	{ FRAMEKEY|ControlMask|ShiftMask, XK_Up,      collapseframe, {.ui = 1} },
	{ FRAMEKEY|ControlMask|ShiftMask, XK_Down,    collapseframe, {.ui = 3} },
	{ FRAMEKEY,           XK_v,     setinframelayout,{.v = &framelayouts[0]} },
	{ FRAMEKEY,           XK_h,     setinframelayout,{.v = &framelayouts[1]} },
	{ FRAMEKEY,           XK_m,     setinframelayout,{.v = &framelayouts[2]} },
	{ FRAMEKEY,           XK_g,     setinframelayout,{.v = &framelayouts[3]} },
	{ FRAMEKEY|ControlMask,           XK_Right, moveframeborder, {.i = +50} },
	{ FRAMEKEY|ControlMask,           XK_Up,    moveframeborder, {.i = +51} },
	{ FRAMEKEY|ControlMask,           XK_Left,  moveframeborder, {.i = +52} },
	{ FRAMEKEY|ControlMask,           XK_Down,  moveframeborder, {.i = +53} },
	{ FRAMEKEY|ControlMask,           XK_h, moveframeborder, {.i = -50} },
	{ FRAMEKEY|ControlMask,           XK_j,    moveframeborder, {.i = -51} },
	{ FRAMEKEY|ControlMask,           XK_l,  moveframeborder, {.i = -52} },
	{ FRAMEKEY|ControlMask,           XK_k,  moveframeborder, {.i = -53} },
	{ FRAMEKEY,             XK_i,    incnclientsframe, {.i = +1} },
	{ FRAMEKEY,             XK_d,    incnclientsframe, {.i = -1} },
};

/* button definitions */
/* click can be ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

